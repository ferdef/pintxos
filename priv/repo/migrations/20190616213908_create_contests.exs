defmodule Pintxos.Repo.Migrations.CreateContests do
  use Ecto.Migration

  def change do
    create table(:contests) do
      add :name, :string
      add :date, :date

      timestamps()
    end

  end
end
