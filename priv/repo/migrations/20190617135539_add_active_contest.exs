defmodule Pintxos.Repo.Migrations.AddActiveContest do
  use Ecto.Migration

  def change do
    alter table(:contests) do
      add :is_active, :boolean, default: false
    end
  end
end
