defmodule Pintxos.Repo.Migrations.CreatePintxos do
  use Ecto.Migration

  def change do
    create table(:pintxos) do
      add :name, :string
      add :description, :text
      add :contest, references(:contests, on_delete: :nothing)
      add :owner, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:pintxos, [:contest])
    create index(:pintxos, [:owner])
  end
end
