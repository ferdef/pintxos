# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :pintxos,
  ecto_repos: [Pintxos.Repo]

# Configures the endpoint
config :pintxos, PintxosWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "zDEwDZ9P7rb1e99Nzq/AZppu4/VD+palVvJCPYruQUOYFwyuI0H3/Oh4fjHlwOiB",
  render_errors: [view: PintxosWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Pintxos.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :pintxos, PintxosWeb.Gettext, locales: ~w(en es), default_locale: "es"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
