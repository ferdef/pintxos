defmodule Pintxos.Event.Contest do
  use Ecto.Schema
  import Ecto.Changeset

  schema "contests" do
    field :date, :date
    field :name, :string
    field :is_active, :boolean, default: false

    timestamps()
  end

  @doc false
  def changeset(contest, attrs) do
    contest
    |> cast(attrs, [:name, :date, :is_active])
    |> validate_required([:name, :date])
  end
end
