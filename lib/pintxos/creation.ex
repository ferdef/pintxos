defmodule Pintxos.Creation do
  @moduledoc """
  The Creation context.
  """

  import Ecto.Query, warn: false
  alias Pintxos.Repo

  alias Pintxos.Creation.Pintxo

  @doc """
  Returns the list of pintxos.

  ## Examples

      iex> list_pintxos()
      [%Pintxo{}, ...]

  """
  def list_pintxos do
    Repo.all(Pintxo)
  end

  @doc """
  Gets a single pintxo.

  Raises `Ecto.NoResultsError` if the Pintxo does not exist.

  ## Examples

      iex> get_pintxo!(123)
      %Pintxo{}

      iex> get_pintxo!(456)
      ** (Ecto.NoResultsError)

  """
  def get_pintxo!(id), do: Repo.get!(Pintxo, id)

  def get_pintxo_for_user(user_id) do
    Repo.get_by(Pintxo, owner: user_id)
  end

  @doc """
  Creates a pintxo.

  ## Examples

      iex> create_pintxo(%{field: value})
      {:ok, %Pintxo{}}

      iex> create_pintxo(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_pintxo(attrs \\ %{}) do
    %Pintxo{}
    |> Pintxo.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a pintxo.

  ## Examples

      iex> update_pintxo(pintxo, %{field: new_value})
      {:ok, %Pintxo{}}

      iex> update_pintxo(pintxo, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_pintxo(%Pintxo{} = pintxo, attrs) do
    pintxo
    |> Pintxo.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Pintxo.

  ## Examples

      iex> delete_pintxo(pintxo)
      {:ok, %Pintxo{}}

      iex> delete_pintxo(pintxo)
      {:error, %Ecto.Changeset{}}

  """
  def delete_pintxo(%Pintxo{} = pintxo) do
    Repo.delete(pintxo)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking pintxo changes.

  ## Examples

      iex> change_pintxo(pintxo)
      %Ecto.Changeset{source: %Pintxo{}}

  """
  def change_pintxo(%Pintxo{} = pintxo) do
    Pintxo.changeset(pintxo, %{})
  end
end
