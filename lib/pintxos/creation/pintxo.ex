defmodule Pintxos.Creation.Pintxo do
  use Ecto.Schema
  import Ecto.Changeset

  schema "pintxos" do
    field :description, :string
    field :name, :string
    field :contest, :id
    field :owner, :id


    timestamps()
  end

  @doc false
  def changeset(pintxo, attrs) do
    pintxo
    |> cast(attrs, [:name, :description, :contest, :owner])
    |> validate_required([:name, :description, :contest, :owner])
  end
end
