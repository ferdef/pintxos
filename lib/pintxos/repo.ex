defmodule Pintxos.Repo do
  use Ecto.Repo,
    otp_app: :pintxos,
    adapter: Ecto.Adapters.Postgres
end
