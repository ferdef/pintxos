defmodule PintxosWeb.Plugs.AuthenticateSession do
  @moduledoc "Redirects user to Authentication view if not logged in"
  # import Plug.Conn
  alias PintxosWeb.Router.Helpers, as: Routes
  alias Pintxos.Auth
  import Phoenix.Controller, only: [redirect: 2]

  def init(options) do
    options
  end

  def call(conn, _opts) do
    IO.inspect(conn)
    if Auth.user_signed_in?(conn) do
      conn
    else
      redirect(conn, to: Routes.session_path(conn, :new))
    end
  end
end
