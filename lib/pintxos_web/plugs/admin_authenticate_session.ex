defmodule PintxosWeb.Plugs.AdminAuthenticateSession do
  @moduledoc "Shows error if user is not admin"
  # import Plug.Conn
  alias PintxosWeb.Router.Helpers, as: Routes
  alias Pintxos.Auth
  import Phoenix.Controller, only: [redirect: 2]

  def init(options) do
    options
  end

  def call(conn, _opts) do
    if Auth.user_is_admin?(conn) do
      conn
    else
      redirect(conn, to: Routes.session_path(conn, :new))
    end
  end
end
