defmodule PintxosWeb.PintxoController do
  use PintxosWeb, :controller

  alias Pintxos.Creation
  alias Pintxos.Creation.Pintxo
  alias Pintxos.{Auth, Event}

  def index(conn, _params) do
    if Auth.user_is_admin?(conn) do
      pintxos = Creation.list_pintxos()
      render(conn, "index.html", pintxos: pintxos)
    else
      pintxo = Creation.get_pintxo_for_user(Auth.current_user(conn))
      redirect(conn, to: Routes.pintxo_path(conn, :show, pintxo))
    end
  end

  def new(conn, _params) do
    changeset = Creation.change_pintxo(%Pintxo{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"pintxo" => pintxo_params}) do
    user = Auth.current_user(conn)
    contest = Event.current_contest()
    params = pintxo_params
             |> Map.put("owner", user.id)
             |> Map.put("contest", contest.id)

    case Creation.create_pintxo(params) do
      {:ok, pintxo} ->
        conn
        |> put_flash(:info, "Pintxo created successfully.")
        |> redirect(to: Routes.pintxo_path(conn, :show, pintxo))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, _params) do
    with user <- Auth.current_user(conn),
         pintxo when not is_nil(pintxo) <- Creation.get_pintxo_for_user(user.id)
    do
      render(conn, "show.html", pintxo: pintxo)
    else
      _ ->
        redirect(conn, to: Routes.pintxo_path(conn, :new))
    end
  end

  def edit(conn, %{"id" => id}) do
    pintxo = Creation.get_pintxo!(id)
    changeset = Creation.change_pintxo(pintxo)
    render(conn, "edit.html", pintxo: pintxo, changeset: changeset)
  end

  def update(conn, %{"id" => id, "pintxo" => pintxo_params}) do
    pintxo = Creation.get_pintxo!(id)

    case Creation.update_pintxo(pintxo, pintxo_params) do
      {:ok, pintxo} ->
        conn
        |> put_flash(:info, "Pintxo updated successfully.")
        |> redirect(to: Routes.pintxo_path(conn, :show, pintxo))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", pintxo: pintxo, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    pintxo = Creation.get_pintxo!(id)
    {:ok, _pintxo} = Creation.delete_pintxo(pintxo)

    conn
    |> put_flash(:info, "Pintxo deleted successfully.")
    |> redirect(to: Routes.pintxo_path(conn, :index))
  end
end
