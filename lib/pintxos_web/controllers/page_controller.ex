defmodule PintxosWeb.PageController do
  use PintxosWeb, :controller

  alias Pintxos.Repo
  alias Pintxos.Event

  def index(conn, _params) do
    current_contest = Event.current_contest
    render(conn, "index.html", current_contest: current_contest)
  end
end
