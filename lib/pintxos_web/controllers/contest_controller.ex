defmodule PintxosWeb.ContestController do
  use PintxosWeb, :controller

  alias Pintxos.Repo
  alias Pintxos.Event
  alias Pintxos.Event.Contest

  def index(conn, _params) do
    contests = Event.list_contests()
    render(conn, "index.html", contests: contests)
  end

  def new(conn, _params) do
    changeset = Event.change_contest(%Contest{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"contest" => contest_params}) do
    case Event.create_contest(contest_params) do
      {:ok, contest} ->
        conn
        |> put_flash(:info, "Contest created successfully.")
        |> redirect(to: Routes.contest_path(conn, :show, contest))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    contest = Event.get_contest!(id)
    render(conn, "show.html", contest: contest)
  end

  def edit(conn, %{"id" => id}) do
    contest = Event.get_contest!(id)
    changeset = Event.change_contest(contest)
    render(conn, "edit.html", contest: contest, changeset: changeset)
  end

  def update(conn, %{"id" => id, "contest" => contest_params}) do
    contest = Event.get_contest!(id)

    case Event.update_contest(contest, contest_params) do
      {:ok, contest} ->
        conn
        |> put_flash(:info, "Contest updated successfully.")
        |> redirect(to: Routes.contest_path(conn, :show, contest))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", contest: contest, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    contest = Event.get_contest!(id)
    {:ok, _contest} = Event.delete_contest(contest)

    conn
    |> put_flash(:info, "Contest deleted successfully.")
    |> redirect(to: Routes.contest_path(conn, :index))
  end

  def activate(conn, %{"id" => id}) do
    contest = Event.get_contest!(id)
    Repo.update_all(Contest, set: [is_active: false])
    Event.update_contest(contest, %{is_active: true})

    conn
    |> put_flash(:info, "Contest #{id} activated")
    |> redirect(to: Routes.contest_path(conn, :index))
  end
end
