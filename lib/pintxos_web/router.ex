defmodule PintxosWeb.Router do
  use PintxosWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :auth_browser do
    plug :browser
    plug PintxosWeb.Plugs.AuthenticateSession, protected: true
  end

  pipeline :admin_auth_browser do
    plug :browser
    plug PintxosWeb.Plugs.AdminAuthenticateSession, protected: true
  end


  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", PintxosWeb do
    pipe_through :browser

    resources "/sessions", SessionController, only: [:new, :create]
  end

  scope "/", PintxosWeb do
    pipe_through :auth_browser

    get "/", PageController, :index

    delete "/sign_out", SessionController, :delete

    resources "/pintxos", PintxoController #, only: [:new, :create, :show]
  end

  scope "/", PintxosWeb do
    pipe_through :admin_auth_browser

    resources "/contests", ContestController
    put "/contests/activate/:id", ContestController, :activate
  end

  # Other scopes may use custom stacks.
  # scope "/api", PintxosWeb do
  #   pipe_through :api
  # end
end
