defmodule Pintxos.CreationTest do
  use Pintxos.DataCase

  alias Pintxos.Creation

  describe "pintxos" do
    alias Pintxos.Creation.Pintxo

    @valid_attrs %{description: "some description", name: "some name"}
    @update_attrs %{description: "some updated description", name: "some updated name"}
    @invalid_attrs %{description: nil, name: nil}

    def pintxo_fixture(attrs \\ %{}) do
      {:ok, pintxo} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Creation.create_pintxo()

      pintxo
    end

    test "list_pintxos/0 returns all pintxos" do
      pintxo = pintxo_fixture()
      assert Creation.list_pintxos() == [pintxo]
    end

    test "get_pintxo!/1 returns the pintxo with given id" do
      pintxo = pintxo_fixture()
      assert Creation.get_pintxo!(pintxo.id) == pintxo
    end

    test "create_pintxo/1 with valid data creates a pintxo" do
      assert {:ok, %Pintxo{} = pintxo} = Creation.create_pintxo(@valid_attrs)
      assert pintxo.description == "some description"
      assert pintxo.name == "some name"
    end

    test "create_pintxo/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Creation.create_pintxo(@invalid_attrs)
    end

    test "update_pintxo/2 with valid data updates the pintxo" do
      pintxo = pintxo_fixture()
      assert {:ok, %Pintxo{} = pintxo} = Creation.update_pintxo(pintxo, @update_attrs)
      assert pintxo.description == "some updated description"
      assert pintxo.name == "some updated name"
    end

    test "update_pintxo/2 with invalid data returns error changeset" do
      pintxo = pintxo_fixture()
      assert {:error, %Ecto.Changeset{}} = Creation.update_pintxo(pintxo, @invalid_attrs)
      assert pintxo == Creation.get_pintxo!(pintxo.id)
    end

    test "delete_pintxo/1 deletes the pintxo" do
      pintxo = pintxo_fixture()
      assert {:ok, %Pintxo{}} = Creation.delete_pintxo(pintxo)
      assert_raise Ecto.NoResultsError, fn -> Creation.get_pintxo!(pintxo.id) end
    end

    test "change_pintxo/1 returns a pintxo changeset" do
      pintxo = pintxo_fixture()
      assert %Ecto.Changeset{} = Creation.change_pintxo(pintxo)
    end
  end
end
