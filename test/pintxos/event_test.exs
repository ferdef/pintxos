defmodule Pintxos.EventTest do
  use Pintxos.DataCase

  alias Pintxos.Event

  describe "contests" do
    alias Pintxos.Event.Contest

    @valid_attrs %{date: ~D[2010-04-17], name: "some name"}
    @update_attrs %{date: ~D[2011-05-18], name: "some updated name"}
    @invalid_attrs %{date: nil, name: nil}

    def contest_fixture(attrs \\ %{}) do
      {:ok, contest} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Event.create_contest()

      contest
    end

    test "list_contests/0 returns all contests" do
      contest = contest_fixture()
      assert Event.list_contests() == [contest]
    end

    test "get_contest!/1 returns the contest with given id" do
      contest = contest_fixture()
      assert Event.get_contest!(contest.id) == contest
    end

    test "create_contest/1 with valid data creates a contest" do
      assert {:ok, %Contest{} = contest} = Event.create_contest(@valid_attrs)
      assert contest.date == ~D[2010-04-17]
      assert contest.name == "some name"
    end

    test "create_contest/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Event.create_contest(@invalid_attrs)
    end

    test "update_contest/2 with valid data updates the contest" do
      contest = contest_fixture()
      assert {:ok, %Contest{} = contest} = Event.update_contest(contest, @update_attrs)
      assert contest.date == ~D[2011-05-18]
      assert contest.name == "some updated name"
    end

    test "update_contest/2 with invalid data returns error changeset" do
      contest = contest_fixture()
      assert {:error, %Ecto.Changeset{}} = Event.update_contest(contest, @invalid_attrs)
      assert contest == Event.get_contest!(contest.id)
    end

    test "delete_contest/1 deletes the contest" do
      contest = contest_fixture()
      assert {:ok, %Contest{}} = Event.delete_contest(contest)
      assert_raise Ecto.NoResultsError, fn -> Event.get_contest!(contest.id) end
    end

    test "change_contest/1 returns a contest changeset" do
      contest = contest_fixture()
      assert %Ecto.Changeset{} = Event.change_contest(contest)
    end
  end
end
