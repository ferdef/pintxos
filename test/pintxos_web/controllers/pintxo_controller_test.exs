defmodule PintxosWeb.PintxoControllerTest do
  use PintxosWeb.ConnCase

  alias Pintxos.Creation

  @create_attrs %{description: "some description", name: "some name"}
  @update_attrs %{description: "some updated description", name: "some updated name"}
  @invalid_attrs %{description: nil, name: nil}

  def fixture(:pintxo) do
    {:ok, pintxo} = Creation.create_pintxo(@create_attrs)
    pintxo
  end

  describe "index" do
    test "lists all pintxos", %{conn: conn} do
      conn = get(conn, Routes.pintxo_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Pintxos"
    end
  end

  describe "new pintxo" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.pintxo_path(conn, :new))
      assert html_response(conn, 200) =~ "New Pintxo"
    end
  end

  describe "create pintxo" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.pintxo_path(conn, :create), pintxo: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.pintxo_path(conn, :show, id)

      conn = get(conn, Routes.pintxo_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Pintxo"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.pintxo_path(conn, :create), pintxo: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Pintxo"
    end
  end

  describe "edit pintxo" do
    setup [:create_pintxo]

    test "renders form for editing chosen pintxo", %{conn: conn, pintxo: pintxo} do
      conn = get(conn, Routes.pintxo_path(conn, :edit, pintxo))
      assert html_response(conn, 200) =~ "Edit Pintxo"
    end
  end

  describe "update pintxo" do
    setup [:create_pintxo]

    test "redirects when data is valid", %{conn: conn, pintxo: pintxo} do
      conn = put(conn, Routes.pintxo_path(conn, :update, pintxo), pintxo: @update_attrs)
      assert redirected_to(conn) == Routes.pintxo_path(conn, :show, pintxo)

      conn = get(conn, Routes.pintxo_path(conn, :show, pintxo))
      assert html_response(conn, 200) =~ "some updated description"
    end

    test "renders errors when data is invalid", %{conn: conn, pintxo: pintxo} do
      conn = put(conn, Routes.pintxo_path(conn, :update, pintxo), pintxo: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Pintxo"
    end
  end

  describe "delete pintxo" do
    setup [:create_pintxo]

    test "deletes chosen pintxo", %{conn: conn, pintxo: pintxo} do
      conn = delete(conn, Routes.pintxo_path(conn, :delete, pintxo))
      assert redirected_to(conn) == Routes.pintxo_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.pintxo_path(conn, :show, pintxo))
      end
    end
  end

  defp create_pintxo(_) do
    pintxo = fixture(:pintxo)
    {:ok, pintxo: pintxo}
  end
end
